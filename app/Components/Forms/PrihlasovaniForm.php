<?php
/* Created by Dominik Miškovec | Date: 09.05.2020 */

namespace App\Forms;


use app\Model\Prihlasovani;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Security\User;

class PrihlasovaniForm extends Control
{

   public function __construct(Prihlasovani $prihlasovani, User $user)
   {
      $this->prihlasovani = $prihlasovani;
      $this->user = $user;
   }

   public function create() {
      $form = new Form();
      $form->addEmail('email', 'Emailová adresa');
      $form->addPassword('heslo', 'Heslo');
      $form->addSubmit('prihlasit', 'Přihlásit se');

      $form->addProtection();
      $form->onSuccess[] = [$this, 'prihlasit'];

      return $form;
   }

   public function prihlasit(Form $form, \stdClass $data) {
      $this->user->login($data->email, $data->heslo);
   }

   private Prihlasovani $prihlasovani;
   private User $user;
}