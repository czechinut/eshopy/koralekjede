<?php
/* Created by Dominik Miškovec | Date: 09.05.2020 */

namespace App\Forms;


use app\Model\Prihlasovani;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class RegistraceForm extends Control
{

   public function __construct(Prihlasovani $prihlasovani)
   {
      $this->prihlasovani = $prihlasovani;
   }

   public function create() {
      $form = new Form();

      $form->addProtection();

      $form->addGroup('Kontaktní údaje');
      $form->addText('jmeno', _('Jméno'));
      $form->addText('prijmeni', _('Příjmení'));

      $form->addGroup('Přihlašovací údaje');
      $form->addEmail('email', 'Emailová adresa');
      $form->addPassword('password', 'Heslo');
      $form->addPassword('password_znovu', 'Heslo (znovu)');

      $form->addGroup('Adresní údaje');
      $form->addText('mesto', 'Město');
      $form->addText('ulice','Ulice (č.p)');
      $form->addInteger('smerovaci_cislo', 'Směrovací číslo');

      $form->setCurrentGroup();

      $form->addCheckbox('newsletter', 'Informovat o novinkách');
      $form->addCheckbox('accept', 'Příjímám podmínky');

      $form->addSubmit('registrovat', 'Registrovat se');

      $form->onSuccess[] = [$this, 'registrovat'];

      return $form;
   }

   public function registrovat(Form $form, \stdClass $data) {
      $this->prihlasovani->registrovat($data);
      $this->flashMessage('Úspěšně zaregistrován! Nyní se můžete přihlásit.');
   }

   private Prihlasovani $prihlasovani;
}