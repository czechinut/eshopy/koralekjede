<?php
/* Created by Dominik Miškovec | Date: 12.05.2020 */

namespace App\Components\Tables;


use app\Model\Zbozi;
use Nette\Application\UI\Control;
use Ublaboo\DataGrid\DataGrid;

class ZboziTable extends Control
{

   public function __construct(Zbozi $zbozi)
   {
      $this->zbozi = $zbozi;
   }

   public function create() {
      $grid = new DataGrid;

      $grid->setPrimaryKey('ID');
      $grid->setDataSource($this->zbozi->ziskejZboziVsechny());

      $grid->addColumnNumber('ID', 'ID Zboží');
      $grid->addColumnText('nazev', 'Název zboží');
      $grid->addColumnNumber('cena', 'Cena (bez DPH)');

      $grid->addColumnNumber('pocet', 'Skladem');

      return $grid;
   }

   private Zbozi $zbozi;
}