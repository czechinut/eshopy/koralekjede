<?php
/* Created by Dominik Miškovec | Date: 08.05.2020 */

namespace app\Model;


use Nette\Database\Context;

class Zbozi
{

   public function __construct(Context $database)
   {
      $this->database = $database;
   }

   public function ziskejTopZbozi() {
      return $this->database
         ->table('zbozi')
         ->limit(3)
         ->fetchAll();
   }

   public function ziskejZbozi($id) {
      if(!$id) {
         return new \Exception('ID nebylo vybráno!');
      }

      return $this->database
         ->table('zbozi')
         ->get($id);
   }

   public function ziskejZboziVsechny() {
      return $this->database
         ->table('zbozi')
         ->fetchAll();
   }

   private Context $database;
}