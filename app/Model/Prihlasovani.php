<?php
/* Created by Dominik Miškovec | Date: 08.05.2020 */

namespace app\Model;

use Granam\CzechVocative\CzechName;
use Nette\Database\Context;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;

class Prihlasovani implements IAuthenticator
{

   const UZIVATELE_TABLE = 'uzivatele';
   const ADRESY_TABLE = 'adresy';

   public function __construct(Context $context, Passwords $passwords)
   {
      $this->database = $context;
      $this->passwords = $passwords;
   }

   public function authenticate(array $credentials): IIdentity
   {
      [$email, $password] = $credentials;

      if(!$email || !$password) {
         throw new \Exception('Nevyplnil jste jeden z důležitých údajů');
      }

      $uzivatel = $this->database->table('uzivatele')
         ->where([
            'email' => $email
         ])->fetch();

      if(!$uzivatel) {
         throw new \Exception('Uživatel nebyl nalezen!');
      }

      if(!$this->passwords->verify($password, $uzivatel->heslo)) {
         throw new \Exception('Heslo neexistuje!');
      }

      $vocative = new CzechName();

      return new Identity($uzivatel->ID, $uzivatel->role, [
         'email' => $email,
         'jmeno' => $uzivatel->jmeno,
         'jmeno_sklonene' => ucfirst($vocative->vocative($uzivatel->jmeno)),
         'prijmeni' => $uzivatel->prijmeni
      ]);
   }

   public function registrovat(\stdClass $data): void {
      if($this->database->table(self::UZIVATELE_TABLE)->where(['email' => $data->email])->count() <= 0) {
         $uzivatel = $this->database->table(self::UZIVATELE_TABLE)
            ->insert([
               'jmeno' => $data->jmeno,
               'prijmeni' => $data->prijmeni,
               'email' => $data->email,
               'heslo' => $this->passwords->hash($data->password)
            ]);

         $this->database->table(self::ADRESY_TABLE)
            ->insert([
               'uzivatel' => $uzivatel->ID,
               'mesto' => $data->mesto,
               'ulice' => $data->ulice,
               'smerovaci_cislo' => $data->smerovaci_cislo
            ]);
      }
   }

   private Context $database;
   private Passwords $passwords;
}