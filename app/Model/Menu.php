<?php
/* Created by Dominik Miškovec | Date: 09.05.2020 */

namespace app\Model;


use Nette\Database\Context;

class Menu
{

   const MAIN = 1;
   const SUB = 2;

   public function __construct(Context $database)
   {
      $this->database = $database;
   }

   public function vratGigaMenu() {
      $main = $this->ziskejMain();

      $pripraveno = [];
      foreach($main as $m) {
         $pripraveno[$m->identifikace] = (object)[
               'nazev' => $m->nazev,
               'suby' => $this->ziskejSub($m->identifikace)
            ];
      }

      return $pripraveno;
   }

   public function vratTopMenu() {
      return $this->database
         ->table('menu_top')
         ->fetchAll();
   }

   private function ziskejMain() {
      return $this->database->table('giga_menu')
         ->where([
            'typ' => self::MAIN
         ])
         ->fetchAll();
   }

   private function ziskejSub($identifikace) {
      $data = $this->database->table('giga_menu')
         ->where([
            'identifikace' => $identifikace,
            'typ' => self::SUB
         ])
         ->fetchAll();

      if(!$data)
         return [];

      return (object)$data;
   }

   private Context $database;
}