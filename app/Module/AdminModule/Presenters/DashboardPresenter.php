<?php
/* Created by Dominik Miškovec | Date: 11.05.2020 */

namespace App\Module\AdminModule\Presenters;


use Nette\Database\Context;
use Ublaboo\DataGrid\DataGrid;

class DashboardPresenter extends MiddlewarePresenter
{

   public function __construct(Context $database)
   {
      $this->database = $database;
   }

   public function renderDefault() {
   }

   protected function createComponentTestTable() {
      $grid = new DataGrid;

      $grid->setPrimaryKey('ID');

      $grid->setDataSource($this->database->table('uzivatele')->fetchAll());

      $grid->addColumnText('jmeno', 'Jméno');
      $grid->addColumnText('prijmeni', 'Příjmení');
      $grid->addColumnText('email', 'Emailová adresa');

      return $grid;
   }

   private Context $database;
}