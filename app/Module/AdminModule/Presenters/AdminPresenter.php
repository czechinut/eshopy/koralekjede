<?php
/* Created by Dominik Miškovec | Date: 11.05.2020 */

namespace App\Module\AdminModule\Presenters;


use App\Forms\PrihlasovaniForm;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;

class AdminPresenter extends MiddlewarePresenter
{

   public function __construct(PrihlasovaniForm $prihlasovaniForm)
   {
      $this->prihlasovaniForm = $prihlasovaniForm;
   }

   public function beforeRender()
   {
      if($this->user->loggedIn)
         if($this->user->isInRole('admin') || $this->user->isInRole('cztm'))
            $this->redirect('Dashboard:default');
         else
            $this->redirectUrl('/');
   }

   protected function createComponentPrihlasovani(): Form
   {
      $prihlasovani = $this->prihlasovaniForm->create();

      return $prihlasovani;
   }

   private PrihlasovaniForm $prihlasovaniForm;
}