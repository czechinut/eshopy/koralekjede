<?php
/* Created by Dominik Miškovec | Date: 12.05.2020 */

namespace App\Module\AdminModule\Presenters;


use App\Components\Tables\ZboziTable;

class ZboziPresenter extends MiddlewarePresenter
{

   public function __construct(ZboziTable $zboziTable)
   {
      $this->zboziTable = $zboziTable;
   }

   public function createComponentZbozi() {
      return $this->zboziTable->create();
   }

   private ZboziTable $zboziTable;
}