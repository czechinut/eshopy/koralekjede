<?php
/* Created by Dominik Miškovec | Date: 12.05.2020 */

namespace App\Module\AdminModule\Presenters;


class MiddlewarePresenter extends \Nette\Application\UI\Presenter
{

   public function beforeRender()
   {
      if(!$this->user->loggedIn)
         if(!$this->user->isInRole('admin') && !$this->user->isInRole('cztm'))
            $this->redirect('Admin:default');
   }
}