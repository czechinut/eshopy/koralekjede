<?php

declare(strict_types=1);

namespace App\Module\FrontModule\Presenters;

use App\Forms\PrihlasovaniForm;
use App\Forms\RegistraceForm;
use app\Model\Menu;
use app\Model\Zbozi;
use Nette;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IComponent;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{

   public function __construct(Zbozi $zbozi, Menu $menu)
   {
      $this->zbozi = $zbozi;
      $this->menu = $menu;
   }

   public function beforeRender()
   {
      $this->template->gigaMenu = $this->menu->vratGigaMenu();
      $this->template->topMenu = $this->menu->vratTopMenu();
   }

   public function renderDefault() {
      $this->template->zboziTop = $this->zbozi->ziskejTopZbozi();
   }

   public function renderRegistrace() {
      if($this->user->loggedIn)
         $this->redirect('default');
   }

   public function actionOdhlasit() {
      $this->user->logout();
      $this->redirect('default');
   }

   protected function createComponentPrihlasovaciOkno(): Form
   {
      return $this->prihlasovaniForm->create();
   }

   protected function createComponentRegistrace(): Form
   {
      return $this->registraceForm->create();
   }

   /** @inject RegistraceForm */
   public RegistraceForm $registraceForm;
   /** @inject PrihlasovaniForm */
   public PrihlasovaniForm $prihlasovaniForm;

   private Zbozi $zbozi;
   private Menu $menu;
}
