<?php
/* Created by Dominik Miškovec | Date: 08.05.2020 */

namespace App\Module\FrontModule\resenters;


use app\Model\Zbozi;
use Nette\Application\UI\Presenter;
use Nette\Database\Context;

class ZboziPresenter extends Presenter
{

   public function __construct(Zbozi $zbozi)
   {
      $this->zbozi = $zbozi;
   }

   public function renderDetail(int $id) {
      if(!$id) {
         $this->redirect('Homepage:');
      }

      $this->template->zbozi = $this->zbozi->ziskejZbozi($id);
   }

   private Zbozi $zbozi;
}